require('dotenv').config();

const axios = require('axios');

const getLocation = (address) => {
	const encodedAddress = encodeURIComponent(address);

	return axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${process.env.GEO_KEY}`);
};

exports.getLocation = (req, res, next) => {
	getLocation('nøstegaten 44')
		.then((geoRes) => {
			if (geoRes.data.status === 'ZERO_RESULTS') {
				throw new Error('Unable to look up that address');
			}

			req.location = geoRes.data.results[0];
		}).catch((err) => {
			if (err.code === 'ENOTFOUND') {
				req.error = 'Unable to connect to Google APIs';
			} else {
				req.error = err.message;
			}
		}).then(() => next());
};