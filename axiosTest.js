const express = require('express');

const geoControllers = require('./controllers/geo-controllers');

const app = express();

app.use(geoControllers.getLocation);

app.get('/', (req, res) => {
	// console.log(req.lat, req.lng);

	console.log(req.location.geometry.location)

	res.send(req.location.formatted_address);
});

app.listen(process.env.PORT || 3000, function (err) {
	err ? console.log(err) : console.log(`Running on port ${this.address().port}...`);
});