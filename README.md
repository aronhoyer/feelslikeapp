# Feelsike
Small app that calculates the [wind chill](https://en.wikipedia.org/wiki/Wind_chill) given the temperature and wind speed, or a location.
